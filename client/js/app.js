/**
 * Client side code.
 */

 (function() {
     "use strict";
     var app = angular.module("QuizApp",[]);

     app.controller("QuizCtrl",["$http",QuizCtrl]);
    
     function QuizCtrl($http) {
         var self = this; // vm

         self.quiz = {

         };

         self.answer = {
             id: 0,
             value: "",
             comments: "",
             message: ""
         };
     
    self.initForm = function() {
        $http.get("/popquizes").then(function(result){
            console.log(result);
            self.quiz = result.data;}).catch(function(e){
                console.log(e)
            });
        }

    self.initForm();

    self.submitQuiz = function() {
        console.log("submitQuiz !");
        self.answer.id = self.quiz.id;
        $http.post("/submit-quiz",self.answer).then(function(result){
            console.log(result);
            if(result.data.isCorrect){
                self.answer.message ="It's correct"
            }else{self.answer.message = "WRONG"}}).catch(function(e) {console.log(e);
            });
    };
}
})();
