// Server Side Code

"use strict";
console.log("Starting App in ....");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

var quizes = [{
    id:0,
    qn : "How many books are there in the Harry Potter series?",
    ans1: {name: "5", value:1},
    ans2: {name: "6", value:2},
    ans3: {name: "7", value:3},
    ans4: {name: "8", value:4},
    correctanswer: 3
},
{
    id:1,
    qn : "What is capital of Germany?",
    ans1: {name: "Berlin", value:1},
    ans2: {name: "Frankfurt", value:2},
    ans3: {name: "Munich", value:3},
    ans4: {name: "Hamburg", value:4},
    correctanswer: 1
},
{
    id:2,
    qn : "What is national anthem of Singapore?",
    ans1: {name: "We are Singapore", value:1},
    ans2: {name: "Home", value:2},
    ans3: {name: "Where I belong", value:3},
    ans4: {name: "Majulah Singapura", value:4},
    correctanswer: 4
},
{
    id:3,
    qn : "What is the alter ego of Batman?",
    ans1: {name: "Bruce Lee", value:1},
    ans2: {name: "Bruce Wayne", value:2},
    ans3: {name: "Peter Parker", value:3},
    ans4: {name: "SpongeBob", value:4},
    correctanswer: 2
},
];

app.get("/popquizes", function(req,res){
    function randomInt (low, high) {
        return Math.floor(Math.random() * (high - low) + low);
    }
    res.json(quizes[randomInt(0,4)]);
});

app.post("/submit-quiz", function(req,res){
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    var quiz = req.body;
    var check = quizes[quiz.id];
    if (check.correctanswer == parseInt(quiz.value))
        {
            console.log("You are right!");
            quiz.isCorrect = true;
        } else {
            console.log("Try again!");
            quiz.isCorrect = false;
        }
        res.status(200).json(quiz);
});


app.use(function(req, res) {
    res.send("<h1>!!!! Page not found ! ! </h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});